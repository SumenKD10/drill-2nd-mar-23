/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/
const fs = require("fs");
const path = require("path");
const userAPI = "https://jsonplaceholder.typicode.com/users";
const todoAPI = "https://jsonplaceholder.typicode.com/todos";

function toWriteInFile(fileName, dataPassed) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path.join(__dirname, fileName), dataPassed, (errorGot) => {
      if (errorGot) {
        console.error("Error while Writing a File");
      } else {
        console.log("Data Written inside the File");
      }
    });
  });
}

function allUsers() {
  fetch(userAPI)
    .then((data) => {
      return data.json();
    })
    .then((data) => {
      console.log(data);
      let fileName = "allUsersInApi.json";
      toWriteInFile(fileName, JSON.stringify(data))
        .then((message) => {
          console.log(message);
        })
        .catch((message) => {
          console.log(message);
        });
    })
    .catch((error) => {
      console.log("Error occured while fetching the data");
    });
}

function allTodos() {
  fetch(todoAPI)
    .then((data) => {
      return data.json();
    })
    .then((data) => {
      console.log(data);
      let fileName = "allTodosInApi.json";
      toWriteInFile(fileName, JSON.stringify(data))
        .then((message) => {
          console.log(message);
        })
        .catch((message) => {
          console.log(message);
        });
    })
    .catch((error) => {
      console.log("Error occured while fetching the data");
    });
}

function allUsersThenTodos() {
  fetch(userAPI)
    .then((dataUsers) => {
      return dataUsers.json();
    })
    .then((dataUsers) => {
      console.log(dataUsers);
    })
    .then(() => {
      fetch(todoAPI)
        .then((dataTodo) => {
          return dataTodo.json();
        })
        .then((dataTodo) => {
          console.log(dataTodo);
          let fileName = "allUsersThenTodosInApi.json";
          toWriteInFile(fileName, JSON.stringify(dataTodo))
            .then((message) => {
              console.log(message);
            })
            .catch((message) => {
              console.log(message);
            });
        })
        .catch((error) => {
          console.log("Error occured while fetching the data");
        });
    })
    .catch((error) => {
      console.log("Error occured while fetching the data");
    });
}

allUsers();
allTodos();
allUsersThenTodos();
