const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/

function allRun() {

    function allItems() {
        let allItemNames = items.filter((eachData) => {
            return (eachData.available === true);
        });
        console.log(allItemNames);
    }

    function filterVitaminCItems() {
        let allVitaminCItems = items.filter((eachData) => {
            return (eachData.contains.indexOf("C") > 0);
        });
        console.log(allVitaminCItems);
    }

    function filterVitaminAItems() {
        let allVitaminAItems = items.filter((eachData) => {
            return (eachData.contains.indexOf("A") > 0);
        });
        console.log(allVitaminAItems);
    }

    function objectGroupVitamin() {
        let arrayOfVitamins = items.reduce((allVitamin, eachVitamin) => {
            eachVitamin.contains.split(", ")
                .map((eachElement) => {
                    if (allVitamin[eachElement] === undefined) {
                        allVitamin[eachElement] = [];
                        allVitamin[eachElement].push(eachVitamin.name);
                    } else {
                        allVitamin[eachElement].push(eachVitamin.name);
                    }
                });
            return allVitamin;
        }, {});
        console.log(arrayOfVitamins);
    }

    function groupItemsBasedOnVitamin() {
        items.sort((item1, item2) => {
            if (item1.contains.split("Vitamin").length > item2.contains.split("Vitamin").length) {
                return 1;
            } else {
                return -1;
            }
        });
        console.log(items);
    }
    allItems();
    filterVitaminCItems();
    filterVitaminAItems();
    objectGroupVitamin();
    groupItemsBasedOnVitamin();

}

allRun();
