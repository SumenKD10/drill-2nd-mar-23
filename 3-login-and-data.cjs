/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
*/
const fs = require("fs");

function create2files() {
  let data1 = "This is File 1";
  let data2 = "This is File 2";
  let promise1 = new Promise((resolve1stPromise, reject1stPromise) => {
    fs.writeFile("./file1.txt", data1, (errorGot1) => {
      if (errorGot1) {
        reject1stPromise("Error: " + errorGot1);
      } else {
        resolve1stPromise("Created new file 1");
      }
    });
  });
  promise1.then(
    (message) => {
      console.log(message);
    },
    (errorMessage) => {
      console.log(errorMessage);
    }
  );
  let promise2 = new Promise((resolve2ndPromise, reject2ndPromise) => {
    fs.writeFile("./file2.txt", data2, (errorGot2) => {
      if (errorGot2) {
        reject2ndPromise("Error: " + errorGot2);
      } else {
        resolve2ndPromise("Created new file 2");
      }
    });
  });
  promise2.then(
    (message) => {
      console.log(message);
    },
    (errorMessage) => {
      console.log(errorMessage);
    }
  );
  Promise.allSettled([promise1, promise2]).then(() => {
    setTimeout(() => {
      return new Promise((resolve, reject) => {
        fs.unlink("./file1.txt", (errorGot) => {
          if (errorGot) {
            reject("Error: " + errorGot);
          } else {
            resolve("File 1 Deleted");
          }
        });
      })
        .then(
          (message) => {
            console.log(message);
          },
          (errorMessage) => {
            console.log(errorMessage);
          }
        )
        .then(() => {
          return new Promise((resolve, reject) => {
            fs.unlink("./file2.txt", (errorGot) => {
              if (errorGot) {
                reject("Error: " + errorGot);
              } else {
                resolve("File 2 Deleted");
              }
            });
          });
        })
        .then((message) => {
          console.log(message);
        })
        .catch((message) => {
          console.log(message);
        });
    }, 2 * 1000);
  });
}

/*
Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

function createLipsumDataFile() {
  let dataCollected;
  return new Promise((resolve, reject) => {
    fs.readFile("./lipsum_data.txt", "utf8", (errorGot, dataGot) => {
      if (errorGot) {
        reject("Error: " + errorGot);
      } else {
        dataCollected = dataGot;
        resolve("File Read Successfully");
      }
    });
  })
    .then(
      (message) => {
        console.log(message);
      },
      (errorMessage) => {
        console.log(errorMessage);
      }
    )
    .then(() => {
      return new Promise((resolve, reject) => {
        fs.writeFile(
          "./newFileForlipsumData.txt",
          dataCollected,
          (errorGot) => {
            if (errorGot) {
              reject("File couldn't written");
            } else {
              resolve("File Written Inside newFileForlipsumData.txt");
            }
          }
        );
      });
    })
    .then(
      (message) => {
        console.log(message);
      },
      (errorMessage) => {
        console.log(errorMessage);
      }
    )
    .then(() => {
      return new Promise((resolve, reject) => {
        fs.unlink("lipsum_data.txt", (errorGot) => {
          if (errorGot) {
            reject("Error: " + errorGot);
          } else {
            resolve("lipsum_data Deleted");
          }
        });
      });
    })
    .then((message) => {
      console.log(message);
    })
    .catch((message) => {
      console.log(message);
    });
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
"Login Success"
"Login Failure"
"GetData Success"
"GetData Failure"

Call log data function after each activity to log that activity in the file.
All logged activity must also include the timestamp for that activity.

You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
All calls must be chained unless mentioned otherwise.

*/

function login(user, val) {
  if (val % 2 === 0) {
    return Promise.resolve(user);
  } else {
    return Promise.reject(new Error("User not found"));
  }
}

function getData() {
  return Promise.resolve([
    {
      id: 1,
      name: "Test",
    },
    {
      id: 2,
      name: "Test 2",
    },
  ]);
}

function logData(user, activity) {
  // use promises and fs to save activity in some file
  login(user, activity)
    .then((message) => {
      console.log(message);
      return getData();
    })
    .then((value) => {
      console.log(value);
    })
    .catch((errorMessage) => {
      console.log(errorMessage);
    });
}

create2files();
createLipsumDataFile();
//logData("Sumen", 3);
