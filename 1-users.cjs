const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interest: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

function allRun() {
  function playingVideoGames() {
    let allUsers = Object.entries(users).filter((eachUser) => {
      if (Array.isArray(eachUser[1].interests)) {
        let arrayToFind = eachUser[1].interests[0].split(", ");
        return (
          arrayToFind.includes("Video Games") ||
          arrayToFind.includes("Playing Video Games")
        );
      }
    });
    console.log(allUsers);
  }

  function usersGermany() {
    let allUsers = Object.entries(users).filter((eachUser) => {
      return eachUser[1].nationality === "Germany";
    });
    console.log(allUsers);
  }

  3;
  function sortingDifferentBasis() {
    let desginationRanking = ["Senior", "Developer", "Intern"];
    let age = [20, 10];
    let allLanguages = ["Golang", "Python", "Javascript"];
    let used = [];

    let allGroups = desginationRanking.reduce((allRank, eachRank) => {
      allRank[eachRank] = [...Object.entries(users)].map((eachUser) => {
        if (eachUser[1].desgination.split(" ").includes(eachRank)) {
          if (!used.includes(eachUser[0])) {
            used.push(eachUser[0]);
            return eachUser[0];
          }
        }
      });
      return allRank;
    }, {});
    //console.log(allGroups);

    let finalResultGroupingAfterSorting = Object.entries(allGroups).map(
      (eachGroup) => {
        eachGroup[1] = eachGroup[1].sort((firstElement, secondElement) => {
          if (users[firstElement].age < users[secondElement].age) {
            return 1;
          } else {
            return -1;
          }
        });
        return eachGroup;
      }
    );
    let finalResultGrouping = Object.entries(allGroups).map((eachGroup) => {
      eachGroup[1] = eachGroup[1].filter((eachElement) => {
        return eachElement !== undefined;
      });
      return eachGroup;
    });
    console.log(finalResultGrouping);
  }

  function mastersDegree() {
    let allUsers = Object.entries(users).filter((eachUser) => {
      return eachUser[1].qualification === "Masters";
    });
    console.log(allUsers);
  }

  function groupingLanguage() {
    let allLanguages = ["Golang", "Python", "Javascript"];

    let allGroups = allLanguages.reduce((allLanguage, eachLanguage) => {
      allLanguage[eachLanguage] = Object.entries(users).map((eachUser) => {
        if (eachUser[1].desgination.split(" ").includes(eachLanguage)) {
          return eachUser[0];
        }
      });
      return allLanguage;
    }, {});
    //console.log(allGroups);
    let finalResultGrouping = Object.entries(allGroups).map((eachLanguage) => {
      eachLanguage[1] = eachLanguage[1].filter((eachElement) => {
        return eachElement !== undefined;
      });
      return eachLanguage;
    });
    console.log(finalResultGrouping);
  }

  playingVideoGames();
  usersGermany();
  sortingDifferentBasis();
  mastersDegree();
  groupingLanguage();
}

allRun();
