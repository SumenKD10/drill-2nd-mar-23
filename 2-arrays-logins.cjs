let dataGathered = require('./data_2.cjs');

function allRun() {
    function allAgender() {
        let allAgenderGathered = dataGathered.filter((eachData) => {
            return (eachData.gender === "Agender");
        });
        console.log(allAgenderGathered);
    }

    function splitIPAddress() {
        let splitIPAddress = dataGathered;
        splitIPAddress.forEach((eachData) => {
            eachData.ip_address = eachData.ip_address.split(".")
                .map((eachElement) => Number(eachElement));
        });
        console.log(splitIPAddress);
    }

    function sumAllSecondComponent() {
        let sumOfAllSecondComponentIP = dataGathered.reduce((sum, eachData) => {
            //eachData.ip_address = eachData.ip_address.split(".");
            sum += Number(eachData.ip_address[1]);
            return sum;
        }, 0);
        console.log(sumOfAllSecondComponentIP);
    }

    function sumAllForthComponent() {
        let sumOfAllForthComponentIP = dataGathered.reduce((sum, eachData) => {
            //eachData.ip_address = eachData.ip_address.split(".");
            sum += Number(eachData.ip_address[3]);
            return sum;
        }, 0);
        console.log(sumOfAllForthComponentIP);
    }

    function fullNameNewKey() {
        let fullNameNewKeyObject = dataGathered;
        fullNameNewKeyObject.forEach((eachData) => {
            eachData['Full_Name'] = eachData.first_name + " " + eachData.last_name;
        });
        console.log(fullNameNewKeyObject);
    }

    function filterOrgEmails() {
        let filteredObject = dataGathered.filter((eachData) => {
            return (eachData.email.slice(-3) === "org");
        });
        console.log(filteredObject);
    }

    function typesOfEmails() {
        let filteredObjectOrg = dataGathered.filter((eachData) => {
            return (eachData.email.slice(-3) === "org");
        });
        let filteredObjectAu = dataGathered.filter((eachData) => {
            return (eachData.email.slice(-2) === "au");
        });
        let filteredObjectCom = dataGathered.filter((eachData) => {
            return (eachData.email.slice(-3) === "com");
        });
        console.log("Org Emails : " + Object.entries(filteredObjectOrg).length);
        console.log("Au Emails : " + Object.entries(filteredObjectAu).length);
        console.log("Com Emails : " + Object.entries(filteredObjectCom).length);
    }

    function sortAccordingToFirstName() {
        let allNameData = dataGathered.sort((firstData, secondData) => {
            let variableA = firstData.first_name;
            let variableB = secondData.first_name;
            if (variableA < variableB) {
                return 1;
            } else {
                return -1;
            }
        });
        console.log(allNameData);
    }
    allAgender();
    splitIPAddress();
    sumAllSecondComponent();
    sumAllForthComponent();
    fullNameNewKey();
    filterOrgEmails();
    typesOfEmails();
    sortAccordingToFirstName();
}

allRun();