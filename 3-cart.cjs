const products = [
  {
    shampoo: {
      price: "$50",
      quantity: 4,
    },
    "Hair-oil": {
      price: "$40",
      quantity: 2,
      sealed: true,
    },
    comb: {
      price: "$12",
      quantity: 1,
    },
    utensils: [
      {
        spoons: { quantity: 2, price: "$8" },
      },
      {
        glasses: { quantity: 1, price: "$70", type: "fragile" },
      },
      {
        cooker: { quantity: 4, price: "$900" },
      },
    ],
    watch: {
      price: "$800",
      quantity: 1,
      type: "fragile",
    },
  },
];

/*

Q1. Find all the items with "price" more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

function allRun() {
  function getAllTheProducts(products) {
    let arrayOfArrays = Object.entries(products[0]);
    let allObjects = arrayOfArrays.filter((eachData) => {
      if (Array.isArray(eachData[1])) {
        let findInside = Object.entries(eachData[1]).filter(
          (eachDataInside) => {
            let price = Object.entries(eachDataInside[1])[0][1].price;
            return Number(price.slice(1)) >= 65;
          }
        );
        console.log(findInside);
      } else {
        return Number(eachData[1].price.slice(1)) >= 65;
      }
    });
    console.log(allObjects);
  }

  function getAllTheProductsQuatityOne(products) {
    let arrayOfArrays = Object.entries(products[0]);
    let allObjects = arrayOfArrays.filter((eachData) => {
      if (Array.isArray(eachData[1])) {
        let findInside = Object.entries(eachData[1]).filter(
          (eachDataInside) => {
            let quantityGot = Object.entries(eachDataInside[1])[0][1].quantity;
            return quantityGot > 1;
          }
        );
        console.log(findInside);
      } else {
        return eachData[1].quantity > 1;
      }
    });
    console.log(allObjects);
  }

  function getAllTheProductsFragile(products) {
    let arrayOfArrays = Object.entries(products[0]);
    let allObjects = arrayOfArrays.filter((eachData) => {
      if (Array.isArray(eachData[1])) {
        let findInside = Object.entries(eachData[1]).filter(
          (eachDataInside) => {
            let typeGot = Object.entries(eachDataInside[1])[0][1].type;
            return typeGot === "fragile";
          }
        );
        console.log(findInside);
      } else {
        return eachData[1].type === "fragile";
      }
    });
    console.log(allObjects);
  }

  function getAllTheProductsLeastandMostExpensive(products) {
    let arrayOfArrays = Object.entries(products[0]);
    let allObjectArray = [];
    let allObjects = arrayOfArrays.filter((eachData) => {
      if (Array.isArray(eachData[1])) {
        let findInside = Object.entries(eachData[1]).map((eachDataInside) => {
          return Object.entries(eachDataInside[1])[0];
        });
        allObjectArray.push(...findInside);
      } else {
        allObjectArray.push(eachData);
      }
    });
    allObjectArrayFiltered = allObjectArray
      .filter((eachData) => {
        return eachData[1].quantity === 1;
      })
      .sort((firstData, secondData) => {
        if (
          Number(firstData[1].price.slice(1)) >
          Number(secondData[1].price.slice(1))
        ) {
          return 1;
        } else if (
          Number(firstData[1].price.slice(1)) <
          Number(secondData[1].price.slice(1))
        ) {
          return -1;
        } else {
          return 0;
        }
      });
    console.log("Least Expensive : " + allObjectArrayFiltered[0]);
    console.log(
      "Most Expensive : " +
        allObjectArrayFiltered[allObjectArrayFiltered.length - 1]
    );
  }

  getAllTheProducts(products);
  getAllTheProductsQuatityOne(products);
  getAllTheProductsFragile(products);
  getAllTheProductsLeastandMostExpensive(products);
}
allRun();
