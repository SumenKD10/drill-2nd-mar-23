/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/
let fs = require("fs");

function retrieveData(callbackFunctionForRetrieveData) {
  fs.readFile("data.json", "utf8", (errorGot, detailsGathered) => {
    if (errorGot) {
      console.error("Error:");
      callbackFunctionForRetrieveData(errorGot);
    } else {
      let idToFind = [2, 13, 23];
      let data = JSON.parse(detailsGathered);
      let allIdDetails = data["employees"].filter((eachData) => {
        return idToFind.includes(eachData.id);
      });
      //console.log(allIdDetails);
      fs.writeFile(
        "retrieveDataSolution.json",
        JSON.stringify(allIdDetails),
        (errorGotInside) => {
          if (errorGotInside) {
            console.error("Error:");
            console.error(errorGotInside);
          } else {
            console.log("Answer written inside the file");
            callbackFunctionForRetrieveData();
          }
        }
      );
    }
  });
}

function groupingCompanies(callbackForGroupingCompanies) {
  fs.readFile("data.json", "utf8", (errorGot, detailsGathered) => {
    if (errorGot) {
      console.error("Error:");
      callbackForGroupingCompanies(errorGot);
    } else {
      let data = JSON.parse(detailsGathered);
      let allDetails = data["employees"].reduce((allGroups, eachData) => {
        //console.log(eachData.company);
        if (allGroups[eachData["company"]] === undefined) {
          allGroups[eachData["company"]] = [];
          allGroups[eachData["company"]].push(eachData);
        } else {
          allGroups[eachData["company"]].push(eachData);
        }
        return allGroups;
      }, {});
      fs.writeFile(
        "groupingCompaniesSolution.json",
        JSON.stringify(allDetails),
        (errorGotInside) => {
          if (errorGotInside) {
            console.error("Error:");
            console.error(errorGotInside);
          } else {
            console.log("Answer written inside the file");
            callbackForGroupingCompanies();
          }
        }
      );
    }
  });
}

function allPowerPuffBrigade(callbackForPowerPuff) {
  fs.readFile("data.json", "utf8", (errorGot, detailsGathered) => {
    if (errorGot) {
      console.error("Error:");
      callbackForPowerPuff(errorGot);
    } else {
      let data = JSON.parse(detailsGathered);
      let allDetails = data["employees"].filter((eachData) => {
        return eachData.company === "Powerpuff Brigade";
      });
      fs.writeFile(
        "allPowerPuffBrigadeSolution.json",
        JSON.stringify(allDetails),
        (errorGotInside) => {
          if (errorGotInside) {
            console.error("Error:");
            console.error(errorGotInside);
          } else {
            console.log("Answer written inside the file");
            callbackForPowerPuff();
          }
        }
      );
    }
  });
}

function removeEntryID2(callbackForRemoveEntryID2) {
  fs.readFile("data.json", "utf8", (errorGot, detailsGathered) => {
    if (errorGot) {
      console.error("Error:");
      callbackForRemoveEntryID2(errorGot);
    } else {
      let data = JSON.parse(detailsGathered);
      let allDetails = data["employees"].filter((eachData) => {
        return eachData.id !== 2;
      });
      fs.writeFile(
        "removeEntryID2Solution.json",
        JSON.stringify(allDetails),
        (errorGotInside) => {
          if (errorGotInside) {
            console.error("Error:");
            console.error(errorGotInside);
          } else {
            console.log("Answer written inside the file");
            callbackForRemoveEntryID2();
          }
        }
      );
    }
  });
}

function sortData(callbackForSortData) {
  fs.readFile("data.json", "utf8", (errorGot, detailsGathered) => {
    if (errorGot) {
      console.error("Error:");
      callbackForSortData(errorGot);
    } else {
      let data = JSON.parse(detailsGathered);
      let allDetails = data["employees"].sort((firstData, secondData) => {
        if (firstData.company > secondData.company) {
          return 1;
        } else if (firstData.company < secondData.company) {
          return -1;
        } else {
          if (firstData.id > secondData.id) {
            return 1;
          } else if (firstData.id < secondData.id) {
            return -1;
          } else {
            return 0;
          }
        }
      });
      fs.writeFile(
        "sortDataSolution.json",
        JSON.stringify(allDetails),
        (errorGotInside) => {
          if (errorGotInside) {
            console.error("Error:");
            console.error(errorGotInside);
          } else {
            console.log("Answer written inside the file");
            callbackForSortData();
          }
        }
      );
    }
  });
}

function idSwapCompany(callbackForIdSwapCompany) {
  fs.readFile("data.json", "utf8", (errorGot, detailsGathered) => {
    if (errorGot) {
      console.error("Error:");
      callbackForIdSwapCompany(errorGot);
    } else {
      let data = JSON.parse(detailsGathered);
      let idFirst = data["employees"].indexOf(
        data["employees"].find((eachData) => {
          return eachData.id === 93;
        })
      );
      let idSecond = data["employees"].indexOf(
        data["employees"].find((eachData) => {
          return eachData.id === 92;
        })
      );
      //   swapping
      let temporaryVariable = data["employees"][idFirst].company;
      data["employees"][idFirst].company = data["employees"][idSecond].company;
      data["employees"][idSecond].company = temporaryVariable;

      let allDetails = data["employees"];

      fs.writeFile(
        "idSwapCompanySolution.json",
        JSON.stringify(allDetails),
        (errorGotInside) => {
          if (errorGotInside) {
            console.error("Error:");
            console.error(errorGotInside);
          } else {
            console.log("Answer written inside the file");
            callbackForIdSwapCompany();
          }
        }
      );
    }
  });
}

function addBirthdayToEvenID(callbackForaddBirthdayToEvenID) {
  fs.readFile("data.json", "utf8", (errorGot, detailsGathered) => {
    if (errorGot) {
      console.error("Error:");
      callbackForaddBirthdayToEvenID(errorGot);
    } else {
      let data = JSON.parse(detailsGathered);
      let date = new Date();
      let allDetails = data["employees"].map((eachData) => {
        if (eachData.id % 2 === 0) {
          eachData["birthday"] = JSON.stringify(date).slice(1, 11);
          return eachData;
        } else {
          return eachData;
        }
      });
      fs.writeFile(
        "addBirthdayToEvenIDSolution.json",
        JSON.stringify(allDetails),
        (errorGotInside) => {
          if (errorGotInside) {
            console.error("Error:");
            console.error(errorGotInside);
          } else {
            console.log("Answer written inside the file");
            callbackForaddBirthdayToEvenID();
          }
        }
      );
    }
  });
}

retrieveData(function (errorGot1) {
  if (errorGot1) {
    console.error("Error while Retrieving Data");
    console.error(errorGot1);
  } else {
    groupingCompanies(function (errorGot2) {
      if (errorGot2) {
        console.error("Error while Grouping");
        console.error(errorGot2);
      } else {
        allPowerPuffBrigade(function (errorGot3) {
          if (errorGot3) {
            console.error("Error while Collecting Powerpuff");
            console.error(errorGot3);
          } else {
            removeEntryID2(function (errorGot4) {
              if (errorGot4) {
                console.error("Error while remove the Id with 2");
                console.error(errorGot4);
              } else {
                sortData(function (errorGot5) {
                  if (errorGot5) {
                    console.error("Error while sorting the data");
                    console.error(errorGot5);
                  } else {
                    idSwapCompany(function (errorGot6) {
                      if (errorGot6) {
                        console.error("Error while swapping the company");
                        console.error(errorGot6);
                      } else {
                        addBirthdayToEvenID(function (errorGot7) {
                          if (errorGot5) {
                            console.error("Error while adding Birthdays");
                            console.error(errorGot7);
                          } else {
                            console.log("Operation Successfull");
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }
});
