let dataGathered = require('./data.cjs');

function all_Web_Developers() {
    let filteredArray = dataGathered.filter((dataFound) => {
        return (dataFound.job.slice(0, 13) === "Web Developer");
    });
    console.log(filteredArray);
}

function changeSalary() {
    let changedArrayOfObject = dataGathered;
    changedArrayOfObject.forEach((eachData) => {
        eachData.salary = Number(eachData.salary.slice(1,));
        return eachData;
    });
    console.log(changedArrayOfObject);
}

function correctEachSalary() {
    let addedNewKeyObject = dataGathered;
    addedNewKeyObject.forEach((eachData) => {
        eachData.correctedSalary = 10000 * Number(eachData.salary.slice(1,));
    });
    console.log(addedNewKeyObject);
}

function sumSalary() {
    let sumSalaryResult = dataGathered.reduce((sumSalary, eachData) => {
        sumSalary += Number(eachData.salary.slice(1,));
        return sumSalary;
    }, 0);
    console.log(sumSalaryResult);
}

function sumSalaryCountry() {
    let sumSalaryResult = dataGathered.reduce((sumSalary, eachData) => {
        if (sumSalary[eachData.location] !== undefined) {
            sumSalary[eachData.location] += Number(eachData.salary.slice(1,));
        } else {
            sumSalary[eachData.location] = Number(eachData.salary.slice(1,));
        }
        return sumSalary;
    }, {});
    console.log(sumSalaryResult);
}

function avgSalaryCountry() {
    let sumSalaryResult = dataGathered.reduce((sumSalary, eachData) => {
        if (sumSalary[eachData.location] !== undefined) {
            sumSalary[eachData.location]['salary'] += Number(eachData.salary.slice(1,));
            sumSalary[eachData.location]['count'] += 1;

        } else {
            sumSalary[eachData.location] = {};
            sumSalary[eachData.location]['salary'] = Number(eachData.salary.slice(1,));
            sumSalary[eachData.location]['count'] = 1;
        }
        return sumSalary;
    }, {});

    let answerObject = {};
    Object.keys(sumSalaryResult).map((eachInfo) => {
        answerObject[eachInfo] = sumSalaryResult[eachInfo].salary / sumSalaryResult[eachInfo].count;
    });
    console.log(answerObject);
}

avgSalaryCountry();