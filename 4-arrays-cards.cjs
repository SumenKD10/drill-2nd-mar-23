let dataGathered = [
  {
    id: 1,
    card_number: "5602221055053843723",
    card_type: "china-unionpay",
    issue_date: "5/25/2021",
    salt: "x6ZHoS0t9vIU",
    phone: "339-555-5239",
  },
  {
    id: 2,
    card_number: "3547469136425635",
    card_type: "jcb",
    issue_date: "12/18/2021",
    salt: "FVOUIk",
    phone: "847-313-1289",
  },
  {
    id: 3,
    card_number: "5610480363247475108",
    card_type: "china-unionpay",
    issue_date: "5/7/2021",
    salt: "jBQThr",
    phone: "348-326-7873",
  },
  {
    id: 4,
    card_number: "374283660946674",
    card_type: "americanexpress",
    issue_date: "1/13/2021",
    salt: "n25JXsxzYr",
    phone: "599-331-8099",
  },
  {
    id: 5,
    card_number: "67090853951061268",
    card_type: "laser",
    issue_date: "3/18/2021",
    salt: "Yy5rjSJw",
    phone: "850-191-9906",
  },
  {
    id: 6,
    card_number: "560221984712769463",
    card_type: "china-unionpay",
    issue_date: "6/29/2021",
    salt: "VyyrJbUhV60",
    phone: "683-417-5044",
  },
  {
    id: 7,
    card_number: "3589433562357794",
    card_type: "jcb",
    issue_date: "11/16/2021",
    salt: "9M3zon",
    phone: "634-798-7829",
  },
  {
    id: 8,
    card_number: "5602255897698404",
    card_type: "china-unionpay",
    issue_date: "1/1/2021",
    salt: "YIMQMW",
    phone: "228-796-2347",
  },
  {
    id: 9,
    card_number: "3534352248361143",
    card_type: "jcb",
    issue_date: "4/28/2021",
    salt: "zj8NhPuUe4I",
    phone: "228-796-2347",
  },
  {
    id: 10,
    card_number: "4026933464803521",
    card_type: "visa-electron",
    issue_date: "10/1/2021",
    salt: "cAsGiHMFTPU",
    phone: "372-887-5974",
  },
];

/* 

    1. Find all card numbers whose sum of all the even position digits is odd.
    2. Find all cards that were issued before June.
    3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
    4. Add a new field to each card to indicate if the card is valid or not.
    5. Invalidate all cards issued before March.
    6. Sort the data into ascending order of issue date.
    7. Group the data in such a way that we can identify which cards were assigned in which months.

    Use method chaining to solve #3, #4, #5, #6 and #7.

    NOTE: Do not change the name of this file 
*/

function allRun() {
  function sumAllEven() {
    let foundCardNumbers = [];
    let finalSumObject = dataGathered.filter((eachData) => {
      let count = 0;
      let sumofAllEvenDigits = eachData.card_number
        .split("")
        .reduce((sumOfAll, eachDigit) => {
          if (count % 2 === 1) {
            sumOfAll += Number(eachDigit);
          }
          count++;
          return sumOfAll;
        }, 0);
      //console.log(sumofAllEvenDigits);
      if (sumofAllEvenDigits % 2 === 1) {
        foundCardNumbers.push(eachData.card_number);
        return eachData;
      }
    });
    console.log(foundCardNumbers);
  }

  function cardIssuedBeforeJune() {
    let finalFoundObject = dataGathered.filter((eachData) => {
      let foundMonth = eachData.issue_date.split("/")[0];
      if (Number(foundMonth) < 6) {
        return eachData;
      }
    });
    console.log(finalFoundObject);
  }

  //   function generateCVV() {
  //     let newObject = dataGathered.filter((eachData) => {
  //       eachData["CVV"] = Math.floor(Math.random() * 900 + 100);
  //       return eachData;
  //     });
  //     console.log(newObject);
  //   }

  // //   function validityCard(){
  // //     let
  // //   }

  // function invalidateCards(){
  //     let madeObjectAfter = dataGathered.filter((eachData)=>{
  //         let foundMonth = eachData.issue_date.split("/")[0];
  //         if(foundMonth < 3){
  //             eachData['validity'] = 'invalid';
  //         } else {
  //             eachData['validity'] = 'valid';
  //         }
  //         return eachData;
  //     });
  //     console.log(madeObjectAfter);
  // }

  // function sortAccIssueDate(){
  //     let sortedObject = dataGathered.map((eachData)=>{
  //         let splitDate = eachData.issue_date.split("/");
  //         eachData['dateValue'] = Number(splitDate[0] + "."+splitDate[1]);
  //         console.log(eachData['dateValue']);
  //     })
  //     .sort((firstData,secondData)=> {
  //         if(firstData.dateValue > secondData.dateValue) {
  //             return 1;
  //         } else {
  //             return -1;
  //         }
  //     });
  //     console.log(sortedObject);

  function methodChaining() {
    let newObject = dataGathered
      .filter((eachData1) => {
        eachData1["CVV"] = Math.floor(Math.random() * 900 + 100);
        return eachData1;
      })
      .filter((eachData2) => {
        if (eachData2.issue_date.length === 16) {
          eachData2["validity"] = "valid";
        } else {
          eachData2["validity"] = "invalid";
        }
        return eachData2;
      })
      .filter((eachData3) => {
        let foundMonth = eachData3.issue_date.split("/")[0];
        if (foundMonth < 3) {
          eachData3["validity"] = "invalid";
        } else {
          eachData3["validity"] = "valid";
        }
        return eachData3;
      })
      .sort((firstData, secondData) => {
        let firstDate = new Date(firstData.issue_date);
        let secondDate = new Date(secondData.issue_date);
        if (firstDate > secondDate) {
          return 1;
        } else {
          return -1;
        }
      })
      .map((eachData4) => {
        let monthData = [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December",
        ];
        let foundMonth = Number(eachData4.issue_date.split("/")[0]);
        eachData4["month"] = monthData[foundMonth - 1];
        return eachData4;
    })
        .reduce((finalObject,eachObject)=>{
            if(finalObject[eachObject.month] === undefined){
                finalObject[eachObject.month] = [];
                finalObject[eachObject.month].push(eachObject);
                delete eachObject.month;
            } else {
                finalObject[eachObject.month].push(eachObject);
                delete eachObject.month;
            }
            return finalObject;
        },{});
    console.log(newObject);
  }

  sumAllEven();
  cardIssuedBeforeJune();
  //generateCVV();
  //validityCard()
  //invalidateCards();
  //sortAccIssueDate();
  methodChaining();
}
allRun();
